# -*- coding: utf-8 -*-
'''
Copyright 2015 Marian Neagul

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Marian Neagul <neagul@gmail.com>
@contact: marian@info.uvt.ro
@copyright: 2015 Marian Neagul
'''

import time
import logging
import threading
from sleekxmpp.exceptions import IqError, IqTimeout

class XmppClientMonitor(threading.Thread):
    log = logging.getLogger(__name__)
    def __init__(self, accounts, interval=10, timeout=10):
        threading.Thread.__init__(self)
        self._accounts = accounts
        self.setDaemon(True)
        self._interval = interval
        self.timeout = timeout

    def run(self):
        while True:
            time.sleep(5)
            self.check_clients()

    def check_clients(self):
        for name in self._accounts:
            account = self._accounts[name]
            client = account["xmpp"]
            try:
                result = client['xep_0199'].send_ping(client.boundjid.bare, timeout=10)
                if result is False:
                    self.log.error("Failed to ping on account %s. SleekXMPP should reconnect", name)
            except IqTimeout:
                self.log.critical("Failed to ping in expected time (%d)", self.timeout)
