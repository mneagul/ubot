# -*- coding: utf-8 -*-
'''
Copyright 2015 Marian Neagul

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Marian Neagul <neagul@gmail.com>
@contact: marian@info.uvt.ro
@copyright: 2015 Marian Neagul
'''

import logging

from sleekxmpp import ClientXMPP
from sleekxmpp.exceptions import IqError, IqTimeout

class Client(ClientXMPP):
    log = logging.getLogger(__name__)
    def __init__(self, name, user, password, nick, expected_rooms=[]):
        ClientXMPP.__init__(self, user, password)
        self.name = name
        self.nick = nick
        self._expected_rooms = expected_rooms

        self.register_plugin('xep_0030') # Service Discovery
        self.register_plugin('xep_0045') # Multi-User Chat
        self.register_plugin('xep_0199') # XMPP Ping

        self.add_event_handler("session_start", self.session_start)

    def session_start(self, event):
        log = self.log
        log.debug("Sending presence")
        self.send_presence()
        log.debug("Geting roster")

        try:
            roster = self.get_roster()
        except IqError as err:
            log.error('There was an error getting the roster')
            log.error(err.iq['error']['condition'])
            self.disconnect()
        except IqTimeout:
            log.error('Server is taking too long to respond')
            self.disconnect()

        for room_entry in self._expected_rooms:
            room_id = room_entry["room"]
            room_password = room_entry.get("password", None)
            room_nick = room_entry.get("nick", self.nick)
            if room_password:
                self.log.info("Joining room %s with user %s. Without password.", room_id, room_nick)
                self.join_muc(room_id, nick=room_nick, password=room_password, wait=False)
            else:
                self.log.info("Joining room %s with user %s. Without password.", room_id, room_nick)
                self.join_muc(room_id, nick=room_nick, wait=False)


    def is_room(self, jid):
        return str(jid) in self.plugin['xep_0045'].getJoinedRooms()


    def join_muc(self, room, nick=None, password=None, wait=True):
        room_nick = self.nick
        if nick is not None:
            room_nick = nick
        if password is not None:
            self.plugin['xep_0045'].joinMUC(room, room_nick, wait=wait, password=password)
        else:
            self.plugin['xep_0045'].joinMUC(room, room_nick, wait=wait)

    def send_message_to_destination(self, jid, message):
        if self.is_room(jid):
            self.log.debug("Sending message to room: %s", jid)
            self.send_message(mto=jid,mbody=message, mtype='groupchat')
        else:
            self.log.debug("Sending message to account: %s", jid)
            self.send_message(mto=jid,mbody=message)