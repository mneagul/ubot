# -*- coding: utf-8 -*-
'''
Copyright 2015 Marian Neagul

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Marian Neagul <neagul@gmail.com>
@contact: marian@info.uvt.ro
@copyright: 2015 Marian Neagul
'''

import logging
import threading


class BasicDispatcher(object):
    def __init__(self, accounts):
        self._lock = threading.Lock()
        self._accounts = accounts
        self.log = logging.getLogger(__name__)


    def _get_account(self, name):
        return self._accounts.get(name, None)


    def send_message_to_jid(self, account, destination, message):
        self.log.debug("Sending message to jid %s: %s", destination, message)
        xmpp_client = self._get_account(account)["xmpp"]
        xmpp_client.send_message_to_destination(destination, message)

