# -*- coding: utf-8 -*-
'''
Copyright 2015 Marian Neagul

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Marian Neagul <neagul@gmail.com>
@contact: marian@info.uvt.ro
@copyright: 2015 Marian Neagul
'''


import os
import sys
import argparse
import logging
import logging.config
import yaml
import pkg_resources
import urlparse


from pkg_resources import ResourceManager

if sys.version_info < (3, 0):
    from sleekxmpp.util.misc_ops import setdefaultencoding
    setdefaultencoding('utf8')

resource_manager = ResourceManager()
LOGGING_YAML_CONFIG_STR = resource_manager.resource_string(__name__, "data/logging.yaml")

def get_entry_points(name, group):
    entry_points = [i for i in pkg_resources.iter_entry_points(group=group, name=name)]
    return entry_points

def main():

    parser = argparse.ArgumentParser(description='The uBot')
    parser.add_argument('--conf', required=True, type=argparse.FileType('r'))
    parser.add_argument("--logging-config", required=False, default=None, type=argparse.FileType('r'))
    args = parser.parse_args()



    Config = yaml.load(args.conf)

    global_config = {}
    if args.logging_config:
        logging_cfg = yaml.load(args.logging_config.read())
    elif "global" in Config and Config["global"] and "logging-config" in Config["global"]:
        log_cfg_file = os.path.expanduser(Config["global"]["logging-config"])
        print log_cfg_file
        if not os.path.exists(log_cfg_file) or not os.access(log_cfg_file, os.R_OK):
            print >>sys.stderr, "[PANIC] failed to open %s" % (log_cfg_file,)
            sys.exit(1)
        else:
            logging_cfg = yaml.load(open(log_cfg_file).read())
    else:
        logging_cfg = yaml.load(LOGGING_YAML_CONFIG_STR)


    logging.config.dictConfig(logging_cfg)

    log = logging.getLogger(__name__)

    log.debug("System starting as uid/gid: %d/%d", os.getuid(), os.getgid())

    global_config = Config.get("global", {})

    import pwd
    import grp

    if "group" in global_config:
        dest_group = global_config["group"]
        log.info("Switching group to %s", dest_group)
        group_info=None
        try:
            group_info = grp.getgrnam(dest_group)
            try:
                os.setgid(group_info.gr_gid)
                log.debug("setgit operation completed")
            except OSError, e:
                log.error("Failed to switch group to %s(%s): %s", dest_group, group_info.gr_gid, e)
                os._exit(1)
        except KeyError:
            log.critical("failed to lookup group: %s", dest_group)
            os._exit(1)

    if "user" in global_config:
        dest_user = global_config["user"]
        log.info("Switching user to %s", dest_user)
        user_info=None
        try:
            user_info = pwd.getpwnam(dest_user)
            try:
                os.setuid(user_info.pw_uid)
                log.debug("setuid operation completed")
            except OSError, e:
                log.error("Failed to switch user to %s(%s): %s", dest_user, user_info.pw_uid, e)
                os._exit(1)

        except KeyError:
            log.critical("failed to lookup user: %s", dest_user)
            os._exit(1)




    log.debug("Starting...")

    configured_accounts = Config["accounts"]
    from Client import Client
    from monitor import XmppClientMonitor
    from dispatcher import BasicDispatcher


    accounts = {}


    for account in configured_accounts:
        name = account["name"]
        if name is accounts:
            log.error("Account %s already registered", name)
            continue

        client = Client(name, account["username"], account["password"], account["nick"], account.get("rooms", []))
        client.connect()
        client.process(block=False)
        entry = {'xmpp': client}
        accounts[name] = entry

    client_monitor = XmppClientMonitor(accounts)
    client_monitor.start()

    message_dispatcher = BasicDispatcher(accounts)



    log.info("Starting Ingestors")

    inputs = Config.get("input", [])
    for inp in inputs:
        name = inp["name"]
        uri = inp["uri"]
        input_type = uri.split("+", 1)
        if len(input_type) == 1: #
            up = urlparse.urlparse(input_type[0])
            input_type = up.scheme
            input_url = uri
        elif len(input_type) > 2:
            raise NotImplementedError("Situation not supported")
        else:
            input_type, input_url = input_type

        entry_points = get_entry_points(input_type, "ubot.input")

        if len(entry_points) >= 1:
            entry_point = entry_points[0]
            if len(entry_points) > 1:
                log.warn("Multiple handlers for %s found. Using first.", input_type)
        else:
            log.error("No handler for %s found!", input_type)
            os._exit(1)

        handlerClass = None
        try:
            handlerClass = entry_point.load()
        except ImportError, e:
            log.critical("Failed to load handler: %s", e)
            continue

        handler = handlerClass(name, input_url, message_dispatcher)
        handler.start()


    #os._exit(1)


def client():
    import zmq
    parser = argparse.ArgumentParser(description='The uBot')
    parser.add_argument('--url', required=True, type=str)
    parser.add_argument('--account', required=True, type=str)
    parser.add_argument('--message', required=False, default=None, type=str)
    action = parser.add_mutually_exclusive_group(required=True)
    action.add_argument('--to', required=False, default=None, type=str)

    args = parser.parse_args()

    message = ""
    if args.message is None:
        message = sys.stdin.read()
    else:
        message = args.message

    json_msg = {
        'account': args.account,
        'message': message
    }


    json_msg["to"] = args.to


    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.setsockopt(zmq.LINGER, 0)
    socket.connect (args.url)

    socket.send_json(json_msg)

    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)
    if poller.poll(10*500):
        response = socket.recv_json()
    else:
        print >>sys.stderr, "No response received. Message might be lost"
        sys.exit(1)




